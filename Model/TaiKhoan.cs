﻿using System.ComponentModel.DataAnnotations;

namespace FullProject.Model
{
    public class TaiKhoan
    {
        [Key]
        public string TenTaiKhoan { get; set; }
        public string MatKhau {  get; set; }
        public string MaNV { get; set; }
        public string Role {  get; set; }
    }
}
