﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FullProject.Model
{
    public class NhanVien
    {
        [Key]
        public string MaNV { get; set; }
        public string HoTen { get; set; }
        public DateTime NgaySinh { get; set; }
        public string CCCD { get; set; }
        public string SoDienThoai { get; set; }
        public string MailLamViec { get; set; }
        public string ChucVu { get; set; }
        public string MaPhongBan { get; set; }
        public string MaChiNhanh { get; set; }
        public string TrangThai { get; set; }
        public string LoaiNhanVien { get; set; }
        public DateTime BatDauLamViec { get; set; }
        public DateTime ChinhThucLamViec { get; set; }
        public string YeuCauChamCong { get; set; }
        public DateTime NgayNghiViec { get; set; }
        public string GioiTinh { get; set; }
        public string DiaChiTamChu { get; set; }
        public string DiaChiThuongChu { get; set; }
        public string TrinhDoHocVan { get; set; }
        public string STKNganHang { get; set; }
        public string TenNganHang { get; set; }
        public string MaSoThue { get; set; }
        public string MaBaoHiem { get; set; }
    }
}
