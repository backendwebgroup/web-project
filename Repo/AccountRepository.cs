﻿using FullProject.Context;
using FullProject.Model;
using Microsoft.EntityFrameworkCore;

namespace FullProject.Repo
{
    public class AccountRepository
    {
        private readonly LoginDbContext _context;

        public AccountRepository(LoginDbContext context)
        {
            _context = context;
        }

        public async Task<TaiKhoan> GetAccountByUsernameAsync(string username)
        {
            return await _context.TaiKhoan.SingleOrDefaultAsync(a => a.TenTaiKhoan == username);
        }

        public async Task RegisterAccountAsync(TaiKhoan account)
        {
            _context.TaiKhoan.Add(account);
            await _context.SaveChangesAsync();
        }

        public async Task<bool> ManvExistsAsync(string manv)
        {
            bool exists = await _context.NhanVien.AnyAsync(nv => nv.MaNV == manv);
            return exists;
        }

        public async Task<List<TaiKhoan>> GetAllTaiKhoanAsync()
        {
            return await _context.TaiKhoan.ToListAsync();
        }

        public async Task DeleteAccountAsync(string maNV)
        {
            // Find the account by MaNV
            var accountToDelete = await _context.TaiKhoan.FirstOrDefaultAsync(a => a.MaNV == maNV);
            if (accountToDelete != null)
            {
                // Remove the account from the DbSet
                _context.TaiKhoan.Remove(accountToDelete);
                // Save changes to the database
                await _context.SaveChangesAsync();
            }
        }

        public async Task UpdateAccountAsync(TaiKhoan updatedAccount)
        {
            // Find the existing account in the database
            var existingAccount = await _context.TaiKhoan.FirstOrDefaultAsync(a => a.TenTaiKhoan == updatedAccount.TenTaiKhoan);

            if (existingAccount != null)
            {
                // Update the properties of the existing account
                existingAccount.MatKhau = updatedAccount.MatKhau;
                existingAccount.MaNV = updatedAccount.MaNV;
                existingAccount.Role = updatedAccount.Role;

                // Save the changes to the database
                await _context.SaveChangesAsync();
            }
        }
    }

}
